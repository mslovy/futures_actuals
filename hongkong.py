import csv

class Arbitrary:
	def __init__(self):
		self.exchange_rate = 0.83675272		# 汇率

		self.stocks = {
			'hold_rate': 1.0,		# 持仓比例
		}

		self.future_factor = 66

		# self.grid_lines = [40, 30, 20, 10, 0, -10, -20, -30, -40]	# 网格线
		self.grid_lines = [20, 15, 10, 5, 0, -5, -10, -15, -20]

		self.last_value = 0.1 		# 上一次的有效差值
		self.cross_over = 0  		# 上一次穿越的网格线
		self.last_direction = 1 	# 上一次穿越方向 1-向正轴方向 -1-向负轴方向
		self.volume = 10 			# 初始持有的期货手数
		self.dissatisfy_volume = 0 	# 未满足的期货手数，正数-需要买入的量 负数-需要卖出的量
		self.balance_volume = 10 	# 平衡状态的交易量
		# self.difference = -32 		# 理论基差
		self.trade_money = 0 		# 交易金钱数
		self.stock_volume = 0 		# 现货数量

		self.date = '20151216';
		self.differences = {
			'20151231': -20,
			'20151230': -70,
			'20151229': -50,
			'20151228': -40,
			'20151224': -40,
			'20151223': -40,
			'20151222': -50,
			'20151221': -30,
			'20151218': -40,
			'20151217': -70,
			'20151216': -70, # strange
			'20151215': -120,
			'20151214': -110,
			'20151211': -100,
			'20151210': -80,
			'20151209': -90,
			'20151208': -100,
			'20151207': -100,
			'20151204': -80,
			'20151203': -60,
			'20151202': -10,
			'20151201': 50
		}

		self.net_worths = {
			'20151231': 0.9860,
			'20151230': 0.9985,
			'20151229': 0.9968,
			'20151228': 1.0134,
			'20151224': 1.0059,
			'20151223': 0.9910,
			'20151222': 0.9927,
			'20151221': 0.9823,
			'20151218': 0.9850,
			'20151217': 0.9704,
			'20151216': 0.9500,
			'20151215': 0.9462,
			'20151214': 0.9436,
			'20151211': 0.9560,
			'20151210': 0.9654,
			'20151209': 0.9747,
			'20151208': 0.9870,
			'20151207': 0.9886,
			'20151204': 1.0060,
			'20151203': 1.0117,
			'20151202': 1.0019,
			'20151201': 0.9863
		}

		self.index_closes = {
			'20151231': 9659.88,
			'20151230': 9788.91,
			'20151229': 9789.46,
			'20151228': 9953.21,
			'20151224': 9882.95,
			'20151223': 9731.53,
			'20151222': 9746.99,
			'20151221': 9634.41,
			'20151218': 9666.52,
			'20151217': 9538.66,
			'20151216': 9346.54,
			'20151215': 9315.91,
			'20151214': 9308.00,
			'20151211': 9450.49,
			'20151210': 9558.76,
			'20151209': 9660.87,
			'20151208': 9798.19,
			'20151207': 9834.28,
			'20151204': 9987.84,
			'20151203': 10050.36,
			'20151202': 9964.78,
			'20151201': 9790.64
		}

		self.index_close = self.index_closes[self.date]
		self.net_worth = self.net_worths[self.date]
		self.difference = self.differences[self.date]
		

	def prepare_data(self):
		self.index_data = []
		self.future_data = []
		self.stock_data = []

		# 获取指数数据
		with open('data/HSCEI/' + self.date + '/HKHSCEI.csv') as csvfile:
			reader = csv.DictReader(csvfile)
			for row in reader:
				self.index_data.append({
					'time': row['Time'].split(' ')[1],
					'price': float(row['Price'])
				})

		# 获取期货数据
		with open('data/HSIF/' + self.date + '/HIHHIF.csv') as csvfile:
			reader = csv.DictReader(csvfile)
			for row in reader:
				self.future_data.append({
					'time': row['Time'].split(' ')[1],
					'bid_price': float(row['BP1']),
					'bid_volume': int(row['BV1']) * 100,
					'ask_price': float(row['SP1']),
					'ask_volume': int(row['SV1']) * 100
				})

		# 获取现货数据
		with open('data/ETF/' + self.date + '/SH510900_' + self.date + '.csv', encoding = 'gb2312') as csvfile:
			reader = csv.DictReader(csvfile)
			for row in reader:
				self.stock_data.append({
					'time': row['时间'].split(' ')[1],
					'price': float(row['最新价']),
					'bid_prices': [float(row['买一价']), float(row['买二价']), float(row['买三价']), float(row['买四价']), float(row['买五价'])],
					'bid_volumes': [int(row['买一量']), int(row['买二量']), int(row['买三量']), int(row['买四量']), int(row['买五量'])],
					'ask_prices': [float(row['卖一价']), float(row['卖二价']), float(row['卖三价']), float(row['卖四价']), float(row['卖五价'])],
					'ask_volumes': [int(row['卖一量']), int(row['卖二量']), int(row['卖三量']), int(row['卖四量']), int(row['卖五量'])]
				})

	def per_second_calculate(self):
		index_pointer = 0
		future_pointer = 0
		stock_pointer = 0

		for hour in [9, 10, 11, 13, 14, 15]:
		# for hour in [9]:
			for minute in range(0, 61):
				if hour == 9 and minute < 30 or hour == 11 and minute > 30:
					continue
				for second in range(0, 61):
					current_time = self.time_string(hour, minute, second)

					while index_pointer + 1 < len(self.index_data) and self.index_data[index_pointer + 1]['time'] <= current_time:
						index_pointer += 1
					while future_pointer + 1 < len(self.future_data) and self.future_data[future_pointer + 1]['time'] <= current_time:
						future_pointer += 1
					while stock_pointer + 1 < len(self.stock_data) and self.stock_data[stock_pointer + 1]['time'] <= current_time:
						stock_pointer += 1		

					# print(self.index_data[index_pointer])
					# print(self.future_data[future_pointer])
					# print(self.stock_data[stock_pointer])

					current_net_worth = self.calculate_net_worth(self.index_data[index_pointer]['price'])
					# print(current_net_worth)
					available_result = { 'available': False }
					# 方向为正，用期货卖价和现货的买价进行计算
					max_count = min(2 * self.balance_volume - self.volume, self.future_data[future_pointer]['ask_volume'])
					for count in range(1, max_count + 1):
						# contract_value = count * self.index_data[index_pointer]['price'] * self.future_factor * self.exchange_rate * 1.00001
						# estimate_index, estimate_volume = self.calculate_estimate_index(contract_value, self.stock_data[stock_pointer]['bid_volumes'], self.stock_data[stock_pointer]['bid_prices'])
						estimate_index, estimate_volume, estimate_value = self.calculate_estimate_index(count, self.index_data[index_pointer]['price'], current_net_worth, self.stock_data[stock_pointer]['bid_volumes'], self.stock_data[stock_pointer]['bid_prices'])
						# print(count, estimate_index, estimate_volume, estimate_value)
						if estimate_index > 0:
							estimate_value = self.index_data[index_pointer]['price'] * estimate_index / current_net_worth 
							# print(count, self.future_data[future_pointer]['ask_price'], self.index_data[index_pointer]['price'], self.stock_data[stock_pointer]['price'], estimate_value)
							difference = self.future_data[future_pointer]['ask_price'] - estimate_value - self.difference
							result = self.trade(difference, 1, count)
							if result['available'] == True:
								# print("%s 实际基差： %f" % (current_time, difference))
								available_result = result
								available_result['estimate_index'] = estimate_index
								available_result['estimate_volume'] = estimate_volume
								available_result['future_price'] = self.future_data[future_pointer]['ask_price']
								available_result['difference'] = difference
								available_result['stock_price'] = self.stock_data[stock_pointer]['bid_prices'][0]
							else:
								break

					# 方向为负，用期货买价和现货的卖价进行计算
					max_count = min(2 * self.balance_volume - self.volume, self.future_data[future_pointer]['bid_volume'])
					for count in range(1, max_count + 1):
						# contract_value = count * self.index_data[index_pointer]['price'] * self.future_factor * self.exchange_rate * 1.00001
						# estimate_index, estimate_volume = self.calculate_estimate_index(contract_value, self.stock_data[stock_pointer]['ask_volumes'], self.stock_data[stock_pointer]['ask_prices'])
						estimate_index, estimate_volume, estimate_value = self.calculate_estimate_index(count, self.index_data[index_pointer]['price'], current_net_worth, self.stock_data[stock_pointer]['ask_volumes'], self.stock_data[stock_pointer]['ask_prices'])
						# print(count, estimate_index, estimate_volume, estimate_value)
						if estimate_index > 0:
							estimate_value = self.index_data[index_pointer]['price'] * estimate_index / current_net_worth 
							# print(count, self.future_data[future_pointer]['bid_price'], self.index_data[index_pointer]['price'], self.stock_data[stock_pointer]['price'], estimate_value)
							difference = self.future_data[future_pointer]['bid_price'] - estimate_value - self.difference
							result = self.trade(difference, -1, count)
							if result['available'] == True:
								# print("%s 实际基差: %f" % (current_time, difference))
								available_result = result
								available_result['estimate_index'] = estimate_index
								available_result['estimate_volume'] = estimate_volume
								available_result['future_price'] = self.future_data[future_pointer]['bid_price']
								available_result['difference'] = difference
								available_result['stock_price'] = self.stock_data[stock_pointer]['bid_prices'][0]
							else:
								break

					if available_result['available']:
						self.adjust_volume(available_result)			

	def time_string(self, hour, minute, second):
		temp_str = ''
		if hour < 10:
			temp_str += '0'
		temp_str += str(hour) + ':'	

		if minute < 10:
			temp_str += '0'
		temp_str += str(minute) + ':'

		if second < 10:
			temp_str += '0'
		temp_str += str(second)
		return temp_str

	def calculate_net_worth(self, current_price):
		stock_rate = (current_price - self.index_close) / self.index_close
		current_net_worth = self.net_worth * (1 + self.stocks['hold_rate'] * stock_rate)

		return current_net_worth

	def calculate_estimate_index(self, index_volume, index_price, current_net_worth, volumes, prices):
		contract_value = index_volume * index_price * self.future_factor * self.exchange_rate * 1.00001
		estimate_value = 0

		# 跨越现货三挡的交易直接放弃
		if contract_value > (volumes[0] * prices[0] + volumes[1] * prices[1]) * 100.15:
			estimate_index = -1
			estimate_volume = 0
		elif contract_value < volumes[0] * prices[0] * 100.015:
			estimate_index = prices[0]
			estimate_volume = round(contract_value / prices[0] / 100.015) * 100
			estimate_value = index_price * estimate_index / current_net_worth 
		else:
			volume_2 = self.future_factor * self.index_close * self.exchange_rate * index_volume / self.net_worth - volumes[0] * 100
			estimate_volume = round((volumes[0] * 100 + volume_2) / 100) * 100
			estimate_index = (volumes[0] * prices[0] * 100 + volume_2 * prices[1]) * 1.0015 / estimate_volume
			estimate_value = index_price * estimate_index / current_net_worth 

		return 	estimate_index, estimate_volume, estimate_value

	# direction为1表示用期货卖价和现货的买价进行计算，direction为-1表示用期货的买价和现货的卖价进行计算
	def trade(self, current_value, direction, current_volume):
		# 方向不对，就不用进行交易
		if self.last_value > current_value and direction == 1 or self.last_value < current_value and direction == -1:
			return { 'available': False }

		# 处理当前情况只够补偿：进行同方向的买卖操作，且交易手数只够完成未满足部分
		# 这个时候只发生成交量变化和成交补偿量变化
		if direction == self.last_direction:
			if abs(self.dissatisfy_volume) >= current_volume:
				return {
					'available': True,
					'last_value': self.last_value,
					'cross_over': self.cross_over,
					'last_direction': self.last_direction,
					'adjust_volume': direction * current_volume,
					'dissatisfy_volume': self.dissatisfy_volume - direction * current_volume
				}

		# 计算一共有几根线和有效穿越几根
		available_line_above = []			# 计算0轴以上所有有效穿越的网格线
		unavailable_count_above = 0 		# 计算0轴以上除了穿越的网格线以外，到达边界线还需要穿越的网格线数量
		available_line_below = []			# 计算0轴以下所有有效穿越的网格线
		unavailable_count_below = 0 		# 计算0轴以下除了穿越的网格线以外，到达边界线还需要穿越的网格线数量
		for index, line in enumerate(self.grid_lines):	
			# 从正数向0轴方向或者向负轴边界方向，在这个方向上，0轴属于0轴以上的穿越需要穿越的网格线
			if self.last_value > current_value:
				# 网格线 < 现差值 < 上次差值，若网格线大于0，则表示从正数向0轴方向进行穿越；否则则是从负数向负轴边界进行穿越
				if line < current_value:	
					# 从正数方向向0轴方向穿越
					if line >= 0:
						unavailable_count_above += 1
					# 从负数方向向负轴边界方向穿越	
					else:	
						unavailable_count_below += 1

				# 判断有效穿越并记录
				if line < self.last_value and line > current_value and line != self.cross_over:
					if line >= 0: 
						available_line_above.append(line)
					else:
						available_line_below.append(line)	
			# 从负数向0轴方向或者向正轴边界方向，在这个方向上，0轴属于0轴以下的穿越需要穿越的网格线
			elif self.last_value < current_value:				
				# 网格线 > 现差值 > 上次差值，若网格线大于0，则表示从正数向正轴边界方向进行穿越；否则则是从负数向0轴方向进行穿越
				if line > current_value:
					# 从正数向正轴边界方向进行穿越
					if line > 0:
						unavailable_count_above += 1
					# 从负数向0轴方向进行穿越	
					else:	
						unavailable_count_below += 1

				# 判断有效穿越并记录		
				if line > self.last_value and line < current_value and line != self.cross_over:
					if line > 0:
						available_line_above.append(line)
					else:
						available_line_below.append(line)	

		# 记录0轴以上和0轴以下穿越的线
		available_count_above = len(available_line_above)
		available_count_below = len(available_line_below)
		total_count_above = available_count_above + unavailable_count_above
		total_count_below = available_count_below + unavailable_count_below

		# 记录所有穿越的线，由于数组中的网格线是从大到小排列，因此如果是向正轴边界方向移动的穿越需要把数据取反
		cross_lines = []
		if self.last_value > current_value:
			cross_lines = available_line_above + available_line_below
		elif self.last_value < current_value:
			available_line_above.reverse()
			available_line_below.reverse()
			cross_lines = available_line_below + available_line_above

		# 计算理论的变化量
		need_volume = []
		volume_after_compensate = self.volume + self.dissatisfy_volume
		# 特别注意：从正数向下穿越0轴再向负轴边界穿越时，需要计算两次，因为穿越过0轴的交易量和向负轴边界方向穿越的交易量变化不同，下面的两个if有可能同时执行！
		if self.last_value > current_value:
			# 从上向0轴方向穿越
			if available_count_above > 0:
				left_volume = volume_after_compensate - self.balance_volume
				# print("%d %d %d %d" % (2, left_volume, available_count_above, total_count_above))
				need_volume = self.calculate_volume(2, left_volume, available_count_above,  total_count_above)
			# 向-40方向穿越
			if available_count_below > 0:
				# 从正数方向穿越0轴而来，这个时候的量就是平衡时的交易量
				if available_count_above > 0:
					left_volume = self.balance_volume
				# 穿越负轴边界，交易量回归为0
				else:	
					left_volume = volume_after_compensate
				# print("%d %d %d %d" % (4, left_volume, available_count_below, total_count_below))
				need_volume += self.calculate_volume(4, left_volume, available_count_below, total_count_below)
		elif self.last_value < current_value:
			# 从下向0轴方向穿越	
			if available_count_below > 0:
				left_volume = self.balance_volume - volume_after_compensate
				# print("%d %d %d %d" % (3, left_volume, available_count_below, total_count_below))
				need_volume = self.calculate_volume(3, left_volume, available_count_below, total_count_below)	
			# 向40方向穿越
			if available_count_above > 0:
				# 从负数方向穿越0轴而来，这个时候的量就是平衡时的交易量
				if available_count_below > 0:
					left_volume = self.balance_volume
				# 穿越正轴边界，交易量为平衡时交易量的两倍
				else:	
					left_volume = 2 * self.balance_volume - volume_after_compensate
				# print("%d %d %d %d" % (1, left_volume, available_count_above, total_count_above))
				need_volume += self.calculate_volume(1, left_volume, available_count_above, total_count_above)	

		# 需要调整的交易量，注意，这个交易量是满足态下还需要进行的交易量
		adjust_volume = direction * sum(need_volume)
		# print(adjust_volume)

		result = {
			'available': False
		}

		# 如果调整量小于这次可以交易的量，则在之前的计算中已经有更优结果出现，此次计算结果作废
		temp_volume = current_volume * direction
		temp_volume -= self.dissatisfy_volume
		if abs(adjust_volume) >= abs(temp_volume):
			for index, v in enumerate(need_volume):
				# print("%d %d" % (temp_volume, v))
				# 依次从各个网格需要交易的量入手，判断此次实际能交易的量能穿越到哪个网格线
				if v >= abs(temp_volume):
					temp_dissatisfy_volume = direction * v - temp_volume
					last_cross_over = cross_lines[index]
					break
				else:	
					temp_volume = temp_volume - direction * v
		
			result = {
				'available': True,
				'last_value': last_cross_over,
				'cross_over': last_cross_over,
				'last_direction': direction,
				'adjust_volume': current_volume * direction,
				'dissatisfy_volume': temp_dissatisfy_volume
			}
		
		return result

	# direction为1表示向40的方向穿越，为2表示从上向0的方向穿越，为3表示从下向0的方向穿越，为4表示向-40的方向穿越
	def calculate_volume(self, direction, total_volume, available_count, total_count):
		need_volume = []
		if total_count == 4:
			if available_count == 1:
				need_volume = [1]
			elif available_count == 2:
				need_volume = [1, 2]
			elif available_count == 3:
				need_volume = [1, 2, 3]
			elif available_count == 4:
				need_volume = [1, 2, 3, 4]			
		elif total_count == 3:
			if total_volume == 6:
				if available_count == 1:
					need_volume = [1]
				elif available_count == 2:
					need_volume = [1, 2]
				elif available_count == 3:	
					need_volume = [1, 2, 3]
			elif total_volume == 7:
				if available_count == 1:
					need_volume = [1]
				elif available_count == 2:
					need_volume = [1, 2]
				elif available_count == 3:	
					need_volume = [1, 2, 4]
			elif total_volume == 8:	
				if available_count == 1:
					need_volume = [1]
				elif available_count == 2:
					need_volume = [1, 3]
				elif available_count == 3:	
					need_volume = [1, 3, 4]
			elif total_volume == 9:
				if available_count == 1:
					need_volume = [2]
				elif available_count == 2:
					need_volume = [2, 3]
				elif available_count == 3:	
					need_volume = [2, 3, 4]
		elif total_count == 2:
			if total_volume == 3:
				if available_count == 1:
					need_volume = [1]
				elif available_count == 2:
					need_volume = [1, 2]
			elif total_volume == 4:
				if available_count == 1:
					need_volume = [1]
				elif available_count == 2:
					need_volume = [1, 3]
			elif total_volume == 5:	
				if available_count == 1:
					need_volume = [2]
				elif available_count == 2:
					need_volume = [2, 3]
			elif total_volume == 6:
				if available_count == 1:
					need_volume = [2]
				elif available_count == 2:
					need_volume = [2, 4]
			elif total_volume == 7:
				if available_count == 1:
					need_volume = [3]
				elif available_count == 2:
					need_volume = [3, 4]
		elif total_count == 1:
			if available_count == 1:
				need_volume = [total_volume]
		return need_volume			

	def adjust_volume(self, result):
		self.last_value = result['last_value']
		self.cross_over = result['cross_over']
		self.last_direction = result['last_direction']
		self.volume += result['adjust_volume']
		self.dissatisfy_volume = result['dissatisfy_volume']

		if (result['adjust_volume'] > 0):
			# 期货做空，现货做多
			self.trade_money += result['adjust_volume'] * (result['future_price'] * self.future_factor - 18) * self.exchange_rate
			self.trade_money -= result['estimate_index'] * result['estimate_volume'] * 1.00015
			self.stock_volume += result['estimate_volume']
		else:	
			# 期货做多，现货做空
			self.trade_money += result['estimate_index'] * result['estimate_volume'] * 0.99985
			self.trade_money += result['adjust_volume'] * (result['future_price'] * self.future_factor + 18) * self.exchange_rate
			self.stock_volume -= result['estimate_volume']

		temp_trade_money = self.trade_money + self.stock_volume * result['stock_price'] + (10 - self.volume) * result['future_price'] * self.future_factor * self.exchange_rate
		# print("实际基差: %.2f, 期货价格: %d, 现货价格: %.5f, 持有空头手数：%d" % (result['difference'], result['future_price'], result['estimate_index'], self.volume))
		print("实际基差: %.2f, 交易差值: %.2f, 期货数量: %d, 期货价格: %d, 现货数量: %f, 现货价格: %.5f" % (result['difference'], temp_trade_money, result['adjust_volume'], result['future_price'], result['estimate_volume'], result['estimate_index']))
		print("最后一次差: %.2f, 最后一次穿越网格线: %d, 穿越方向：%d, 持有空头手数：%d, 持有多头数: %d, 未满足手数：%d" % (self.last_value, self.cross_over, self.last_direction, self.volume, self.stock_volume, self.dissatisfy_volume))

if __name__ == '__main__':
	ar = Arbitrary()
	ar.prepare_data()
	ar.per_second_calculate()