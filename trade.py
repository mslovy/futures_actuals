from account import Account
from data import Data

class Trade:
	# GRID_LINES = [40, 30, 20, 10, 0, -10, -20, -30, -40]	# 网格线
	GRID_LINES = [20, 15, 10, 5, 0, -5, -10, -15, -20]	# 网格线
	BALANCE_VOLUME = 10		# 平衡状态的持仓量

	def __init__(self, date):
		self.last_difference = 0.1 		# 上一次的差值
		self.cross_over = 0  			# 上一次穿越的网格线
		self.last_direction = 1 		# 上一次穿越方向 1-向正轴方向 -1-向负轴方向
		self.dissatisfy_volume = 0 		# 未满足的期货手数，正数-需要买入的量 负数-需要卖出的量

		self.account = Account()
		self.data = Data(date)
		self.data.prepare_data()
		
		self.hypostatic_differences = {
			'20151231': -20,
			'20151230': -70,
			'20151229': -50,
			'20151228': -40,
			'20151224': -40,
			'20151223': -40,
			'20151222': -50,
			'20151221': -30,
			'20151218': -40,
			'20151217': -70,
			'20151216': -70, # strange
			'20151215': -120,
			'20151214': -110,
			'20151211': -100,
			'20151210': -80,
			'20151209': -90,
			'20151208': -100,
			'20151207': -100,
			'20151204': -100,
			'20151203': -80,
			'20151202': -10,
			'20151201': 30
		}
		self.hypostatic_difference = self.hypostatic_differences[date]

	def per_second_calculate(self):
		for hour in [9, 10, 11, 13, 14]:
		# for hour in [9]:
			for minute in range(0, 60):
				if hour == 9 and minute < 30 or hour == 11 and minute > 30:
					continue
				for second in range(0, 60):
					current_time = self.data.time_string(hour, minute, second)

					index_data = self.data.get_current_index_data(current_time)
					stock_data = self.data.get_current_stock_data(current_time)
					future_data = self.data.get_current_future_data(current_time)

					current_net_worth = self.data.calculate_net_worth(index_data['price'])
					
					available_result = { 'available': False }
					# 方向为正，用期货卖价和现货的买价进行计算 - 卖现货 买期货
					max_count = min(self.account.future_volume, future_data['ask_volume'])
					for count in range(1, max_count + 1):
						result = self.trade_by_volume(-1, count, future_data['ask_price'], index_data['price'], current_net_worth, stock_data['bid_prices'], stock_data['bid_volumes'])
						if result['available'] == True:
							available_result = result
							available_result['stock_prices'] = stock_data['bid_prices']
							available_result['stock_volumes'] = stock_data['bid_volumes']
						else:	
							break

					# 方向为负，用期货买价和现货的卖价进行计算
					max_count = min(2 * self.BALANCE_VOLUME - self.account.future_volume, future_data['bid_volume'])
					for count in range(1, max_count + 1):
						result = self.trade_by_volume(1, count, future_data['bid_price'], index_data['price'], current_net_worth, stock_data['ask_prices'], stock_data['ask_volumes'])
						if result['available'] == True:
							available_result = result
							available_result['stock_prices'] = stock_data['ask_prices']
							available_result['stock_volumes'] = stock_data['ask_volumes']
						else:	
							break

					if available_result['available']:
						self.account.adjust_volume(available_result['future_volume'], available_result['future_price'], available_result['stock_volume'], available_result['stock_price'])	
						
						self.last_difference = available_result['last_difference']
						self.cross_over = available_result['cross_over']
						self.last_direction = available_result['last_direction']
						self.dissatisfy_volume = available_result['dissatisfy_volume']

						print("%s 实际差值: %.2f, 最后一次差: %.2f, 最后一次穿越网格线: %d, 穿越方向：%d, 持有空头手数：%d, 持有多头数: %d, 未满足手数：%d" % (current_time, available_result['estimate_index'], self.last_difference, self.cross_over, self.last_direction, self.account.future_volume, self.account.stock_volume, self.dissatisfy_volume))

	def trade_by_volume(self, direction, future_volume, future_price, index_price, current_net_worth, stock_prices, stock_volumes):
		estimate_price, estimate_volume, estimate_index = self.data.calculate_estimate_index(future_volume, index_price, current_net_worth, stock_volumes, stock_prices)
		
		result = { 'available': False }
		if estimate_price > 0:
			difference = future_price - estimate_index - self.hypostatic_difference
			result = self.trade(difference, direction, future_volume)
			if result['available'] == True:
				result['stock_price'] = estimate_price
				result['stock_volume'] = estimate_volume * direction
				result['future_price'] = future_price
				result['estimate_index'] = difference
		return result

	# direction为1表示用期货卖价和现货的买价进行计算，direction为-1表示用期货的买价和现货的卖价进行计算
	def trade(self, current_difference, direction, current_volume):
		# 方向不对，就不用进行交易
		if self.last_difference > current_difference and direction == 1 or self.last_difference < current_difference and direction == -1:
			return { 'available': False }

		# 处理当前情况只够补偿：进行同方向的买卖操作，且交易手数只够完成未满足部分
		# 这个时候只发生成交量变化和成交补偿量变化
		if direction == self.last_direction:
			if abs(self.dissatisfy_volume) >= current_volume:
				return {
					'available': True,
					'last_difference': self.last_difference,
					'cross_over': self.cross_over,
					'last_direction': self.last_direction,
					'future_volume': direction * current_volume,
					'dissatisfy_volume': self.dissatisfy_volume - direction * current_volume
				}

		# 计算一共有几根线和有效穿越几根
		available_line_above = []			# 计算0轴以上所有有效穿越的网格线
		unavailable_count_above = 0 		# 计算0轴以上除了穿越的网格线以外，到达边界线还需要穿越的网格线数量
		available_line_below = []			# 计算0轴以下所有有效穿越的网格线
		unavailable_count_below = 0 		# 计算0轴以下除了穿越的网格线以外，到达边界线还需要穿越的网格线数量
		for index, line in enumerate(self.GRID_LINES):	
			# 从正数向0轴方向或者向负轴边界方向，在这个方向上，0轴属于0轴以上的穿越需要穿越的网格线
			if self.last_difference > current_difference:
				# 网格线 < 现差值 < 上次差值，若网格线大于0，则表示从正数向0轴方向进行穿越；否则则是从负数向负轴边界进行穿越
				if line < current_difference:	
					# 从正数方向向0轴方向穿越
					if line >= 0:
						unavailable_count_above += 1
					# 从负数方向向负轴边界方向穿越	
					else:	
						unavailable_count_below += 1

				# 判断有效穿越并记录
				if line < self.last_difference and line > current_difference and line != self.cross_over:
					if line >= 0: 
						available_line_above.append(line)
					else:
						available_line_below.append(line)	
			# 从负数向0轴方向或者向正轴边界方向，在这个方向上，0轴属于0轴以下的穿越需要穿越的网格线
			elif self.last_difference < current_difference:				
				# 网格线 > 现差值 > 上次差值，若网格线大于0，则表示从正数向正轴边界方向进行穿越；否则则是从负数向0轴方向进行穿越
				if line > current_difference:
					# 从正数向正轴边界方向进行穿越
					if line > 0:
						unavailable_count_above += 1
					# 从负数向0轴方向进行穿越	
					else:	
						unavailable_count_below += 1

				# 判断有效穿越并记录		
				if line > self.last_difference and line < current_difference and line != self.cross_over:
					if line > 0:
						available_line_above.append(line)
					else:
						available_line_below.append(line)	

		# 记录0轴以上和0轴以下穿越的线
		available_count_above = len(available_line_above)
		available_count_below = len(available_line_below)
		total_count_above = available_count_above + unavailable_count_above
		total_count_below = available_count_below + unavailable_count_below

		# 记录所有穿越的线，由于数组中的网格线是从大到小排列，因此如果是向正轴边界方向移动的穿越需要把数据取反
		cross_lines = []
		if self.last_difference > current_difference:
			cross_lines = available_line_above + available_line_below
		elif self.last_difference < current_difference:
			available_line_above.reverse()
			available_line_below.reverse()
			cross_lines = available_line_below + available_line_above

		# 计算理论的变化量
		need_volume = []
		volume_after_compensate = self.account.future_volume + self.dissatisfy_volume
		# 特别注意：从正数向下穿越0轴再向负轴边界穿越时，需要计算两次，因为穿越过0轴的交易量和向负轴边界方向穿越的交易量变化不同，下面的两个if有可能同时执行！
		if self.last_difference > current_difference:
			# 从上向0轴方向穿越
			if available_count_above > 0:
				left_volume = volume_after_compensate - self.BALANCE_VOLUME
				need_volume = self.calculate_volume(left_volume, available_count_above,  total_count_above)
			# 向-40方向穿越
			if available_count_below > 0:
				# 从正数方向穿越0轴而来，这个时候的量就是平衡时的交易量
				if available_count_above > 0:
					left_volume = self.BALANCE_VOLUME
				# 穿越负轴边界，交易量回归为0
				else:	
					left_volume = volume_after_compensate
				need_volume += self.calculate_volume(left_volume, available_count_below, total_count_below)
		elif self.last_difference < current_difference:
			# 从下向0轴方向穿越	
			if available_count_below > 0:
				left_volume = self.BALANCE_VOLUME - volume_after_compensate
				need_volume = self.calculate_volume(left_volume, available_count_below, total_count_below)	
			# 向40方向穿越
			if available_count_above > 0:
				# 从负数方向穿越0轴而来，这个时候的量就是平衡时的交易量
				if available_count_below > 0:
					left_volume = self.BALANCE_VOLUME
				# 穿越正轴边界，交易量为平衡时交易量的两倍
				else:	
					left_volume = 2 * self.BALANCE_VOLUME - volume_after_compensate
				need_volume += self.calculate_volume(left_volume, available_count_above, total_count_above)	

		# 需要调整的交易量，注意，这个交易量是满足态下还需要进行的交易量
		adjust_volume = direction * sum(need_volume)

		result = {
			'available': False
		}

		# 如果调整量小于这次可以交易的量，则在之前的计算中已经有更优结果出现，此次计算结果作废
		temp_volume = current_volume * direction
		temp_volume -= self.dissatisfy_volume
		if abs(adjust_volume) >= abs(temp_volume):
			for index, v in enumerate(need_volume):
				# print("%d %d" % (temp_volume, v))
				# 依次从各个网格需要交易的量入手，判断此次实际能交易的量能穿越到哪个网格线
				if v >= abs(temp_volume):
					temp_dissatisfy_volume = direction * v - temp_volume
					last_cross_over = cross_lines[index]
					break
				else:	
					temp_volume = temp_volume - direction * v
		
			result = {
				'available': True,
				'last_difference': last_cross_over,
				'cross_over': last_cross_over,
				'last_direction': direction,
				'future_volume': current_volume * direction,
				'dissatisfy_volume': temp_dissatisfy_volume
			}
		
		return result

	def calculate_volume(self, total_volume, cross_count, total_count):
		need_volume = []
		if total_count == 4:
			if cross_count == 1:
				need_volume = [1]
			elif cross_count == 2:
				need_volume = [1, 2]
			elif cross_count == 3:
				need_volume = [1, 2, 3]
			elif cross_count == 4:
				need_volume = [1, 2, 3, 4]			
		elif total_count == 3:
			if total_volume == 6:
				if cross_count == 1:
					need_volume = [1]
				elif cross_count == 2:
					need_volume = [1, 2]
				elif cross_count == 3:	
					need_volume = [1, 2, 3]
			elif total_volume == 7:
				if cross_count == 1:
					need_volume = [1]
				elif cross_count == 2:
					need_volume = [1, 2]
				elif cross_count == 3:	
					need_volume = [1, 2, 4]
			elif total_volume == 8:	
				if cross_count == 1:
					need_volume = [1]
				elif cross_count == 2:
					need_volume = [1, 3]
				elif cross_count == 3:	
					need_volume = [1, 3, 4]
			elif total_volume == 9:
				if cross_count == 1:
					need_volume = [2]
				elif cross_count == 2:
					need_volume = [2, 3]
				elif cross_count == 3:	
					need_volume = [2, 3, 4]
		elif total_count == 2:
			if total_volume == 3:
				if cross_count == 1:
					need_volume = [1]
				elif cross_count == 2:
					need_volume = [1, 2]
			elif total_volume == 4:
				if cross_count == 1:
					need_volume = [1]
				elif cross_count == 2:
					need_volume = [1, 3]
			elif total_volume == 5:	
				if cross_count == 1:
					need_volume = [2]
				elif cross_count == 2:
					need_volume = [2, 3]
			elif total_volume == 6:
				if cross_count == 1:
					need_volume = [2]
				elif cross_count == 2:
					need_volume = [2, 4]
			elif total_volume == 7:
				if cross_count == 1:
					need_volume = [3]
				elif cross_count == 2:
					need_volume = [3, 4]
		elif total_count == 1:
			if cross_count == 1:
				need_volume = [total_volume]
		
		return need_volume			

if __name__ == '__main__':
	trade = Trade('20151207')
	account = Account()
	trade.per_second_calculate()