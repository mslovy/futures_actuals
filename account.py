class Account:
	
	EXCHANGE_RATE = 0.83675272		# 汇率
	FUTURE_FACTOR = 50				# 期货乘数
	HK_COMMISSION_CHARGE_FEE = 18
	CN_COMMISSION_CHARGE_RATE = 0.00015

	def __init__(self):
		self.future_volume = 10 				# 初始持有的期货手数
		self.stock_volume = 0 					# 已成交的现货量
		self.money = 0

	def future_contract(price):
		return (price * Account.FUTURE_FACTOR + Account.HK_COMMISSION_CHARGE_FEE) * Account.EXCHANGE_RATE

	def stock_volume(value, price):
		return round(value / price / (1 + Account.CN_COMMISSION_CHARGE_RATE) / 100) * 100

	def adjust_volume(self, future_volume, future_price, stock_volume, stock_price):	
		self.future_volume += future_volume
		self.stock_volume += stock_volume
		self.money += future_volume * (future_price * self.FUTURE_FACTOR) * self.EXCHANGE_RATE
		self.money -= abs(future_volume) * self.HK_COMMISSION_CHARGE_FEE * self.EXCHANGE_RATE
		self.money -= stock_volume * stock_price 
		self.money -= abs(stock_volume) * stock_price * self.CN_COMMISSION_CHARGE_RATE

		print('期货价格: %.2f, 期货数量: %d, 现货价格: %.6f, 现货数量: %d, 现金: %.2f' % (future_price, self.future_volume, stock_price, self.stock_volume, self.money))

if __name__ == '__main__':
	print(Account.future_contract(9539.46))