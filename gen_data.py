#!/bin/env python
# -*- coding: utf-8 -*-
import csv
import sys
import os

class DataHolder:
  def __init__(self, date, net_worth_close, index_close):
    self.date = date
    self.month = date[0:6]
    #盘中净值/前日净值 = 指数涨幅 + 1, 现货值x = 恒指ETF加权价格 * (盘中指数/净值)
    self.index_to_net_worth_ratio = float(index_close) / float(net_worth_close)

  def prepare_data(self):
    self.index_data = []
    self.future_data = []
    self.stock_data = []

    index_file = 'data/HSCEI/HSCEI/HKHSCEI_%s.csv' % self.date
    futures_file = 'data/HSIF/%s/%s/HIHHIF.csv' % (self.month, self.date)
    spot_file = 'data/Fnd/%s/%s/SH510900_%s.csv' % (self.month, self.date, self.date)
    if not (os.path.exists(index_file) and os.path.exists(futures_file) and os.path.exists(spot_file)):
      print("%s data missing!" % self.date)
      return False

    # 获取指数数据
    with open(index_file) as csvfile:
      reader = csv.DictReader(csvfile)
      for row in reader:
        self.index_data.append({
          'time': row['Time'].split(' ')[1],
          'price': float(row['Price'])
        })

    # 获取期货数据
    with open(futures_file) as csvfile:
      reader = csv.DictReader(csvfile)
      for row in reader:
        data = {
          'time': row['Time'].split(' ')[1],
          'bid_price': float(row['BP1']),
          'bid_volume': int(row['BV1']) * 100,
          'ask_price': float(row['SP1']),
          'ask_volume': int(row['SV1']) * 100
        }
        if (data['bid_volume'] + data['ask_volume']) == 0:
          continue
        data['weighted_price'] = (data['bid_price'] * data['bid_volume'] + data['ask_price'] * data['ask_volume']) / (data['bid_volume'] + data['ask_volume'])
        self.future_data.append(data)

    # 获取现货数据
    with open(spot_file, encoding = 'gb2312') as csvfile:
      reader = csv.DictReader(csvfile)
      for row in reader:
        data = {
          'time': row['时间'].split(' ')[1],
          'price': float(row['最新价']),
          'bid_prices': [float(row['买一价']), float(row['买二价']), float(row['买三价']), float(row['买四价']), float(row['买五价'])],
          'bid_volumes': [int(row['买一量']), int(row['买二量']), int(row['买三量']), int(row['买四量']), int(row['买五量'])],
          'ask_prices': [float(row['卖一价']), float(row['卖二价']), float(row['卖三价']), float(row['卖四价']), float(row['卖五价'])],
          'ask_volumes': [int(row['卖一量']), int(row['卖二量']), int(row['卖三量']), int(row['卖四量']), int(row['卖五量'])]
        }
        if (data['bid_volumes'][0] + data['ask_volumes'][0]) == 0:
          continue
        data['weighted_price'] = (data['bid_prices'][0] * data['bid_volumes'][0] + data['ask_prices'][0] * data['ask_volumes'][0]) / (data['bid_volumes'][0] + data['ask_volumes'][0])
        self.stock_data.append(data)
    return True

  def per_second_calculate(self):
    index_pointer = 0
    future_pointer = 0
    stock_pointer = 0

    with open('output/%s.csv' % self.date, 'w') as f_out:
      f_out.write('Time,Index,Future,Spot\n')
      for hour in [9, 10, 11, 13, 14, 15]:
      # for hour in [9]:
        for minute in range(0, 60):
          if hour == 9 and minute < 30 or hour == 11 and minute > 30:
            continue
          for second in range(0, 60):
            current_time = self.time_string(hour, minute, second)
  
            while index_pointer + 1 < len(self.index_data) and self.index_data[index_pointer + 1]['time'] <= current_time:
              index_pointer += 1
            while future_pointer + 1 < len(self.future_data) and self.future_data[future_pointer + 1]['time'] <= current_time:
              future_pointer += 1
            while stock_pointer + 1 < len(self.stock_data) and self.stock_data[stock_pointer + 1]['time'] <= current_time:
              stock_pointer += 1    
  
           #计算现货estimate_index
            estimate_index = self.stock_data[stock_pointer]['weighted_price'] * self.index_to_net_worth_ratio
  
            f_out.write('%s,%f,%f,%f\n' % (current_time, self.index_data[index_pointer]['price'], self.future_data[future_pointer]['weighted_price'], estimate_index))

  def time_string(self, hour, minute, second):
    temp_str = ''
    if hour < 10:
      temp_str += '0'
    temp_str += str(hour) + ':' 

    if minute < 10:
      temp_str += '0'
    temp_str += str(minute) + ':'

    if second < 10:
      temp_str += '0'
    temp_str += str(second)
    return temp_str


if __name__ == '__main__':
    # 获取前日ETF净值和CEI收盘指数
  net_worth_and_index = {}
  with open('data/net_worth_and_index.csv') as f:
    for line in f.readlines():
      if line.startswith('#'):
        continue
      line = line.strip().split(',')
      date = line[0]
      net_worth_close = line[1]
      index_close = line[2]
      if net_worth_close == '':
        print("%s net_worth_close missing" % date)
        continue
      ar = DataHolder(date, net_worth_close, index_close)
      if ar.prepare_data():
        ar.per_second_calculate()
