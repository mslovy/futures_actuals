import csv
from account import Account

class Data:
	def __init__(self, date):
		self.index_data = []
		self.future_data = []
		self.stock_data = []

		self.index_pointer = 0
		self.future_pointer = 0
		self.stock_pointer = 0

		self.current_date = date

		self.net_worths = {
			'20151231': 0.9860,
			'20151230': 0.9985,
			'20151229': 0.9968,
			'20151228': 1.0134,
			'20151224': 1.0059,
			'20151223': 0.9910,
			'20151222': 0.9927,
			'20151221': 0.9823,
			'20151218': 0.9850,
			'20151217': 0.9704,
			'20151216': 0.9500,
			'20151215': 0.9462,
			'20151214': 0.9436,
			'20151211': 0.9560,
			'20151210': 0.9654,
			'20151209': 0.9747,
			'20151208': 0.9870,
			'20151207': 0.9886,
			'20151204': 1.0060,
			'20151203': 1.0117,
			'20151202': 1.0019,
			'20151201': 0.9863
		}

		self.index_closes = {
			'20151231': 9659.88,
			'20151230': 9788.91,
			'20151229': 9789.46,
			'20151228': 9953.21,
			'20151224': 9882.95,
			'20151223': 9731.53,
			'20151222': 9746.99,
			'20151221': 9634.41,
			'20151218': 9666.52,
			'20151217': 9538.66,
			'20151216': 9346.54,
			'20151215': 9315.91,
			'20151214': 9308.00,
			'20151211': 9450.49,
			'20151210': 9558.76,
			'20151209': 9660.87,
			'20151208': 9798.19,
			'20151207': 9834.28,
			'20151204': 9987.84,
			'20151203': 10050.36,
			'20151202': 9964.78,
			'20151201': 9790.64
		}

	def prepare_data(self):
		# 获取指数数据
		with open('data/HSCEI/' + self.current_date + '/HKHSCEI.csv') as csvfile:
			reader = csv.DictReader(csvfile)
			for row in reader:
				self.index_data.append({
					'time': row['Time'].split(' ')[1],
					'price': float(row['Price'])
				})

		# 获取期货数据
		with open('data/HSIF/' + self.current_date + '/HIHHIF.csv') as csvfile:
			reader = csv.DictReader(csvfile)
			for row in reader:
				self.future_data.append({
					'time': row['Time'].split(' ')[1],
					'bid_price': float(row['BP1']),
					'bid_volume': int(row['BV1']) * 100,
					'ask_price': float(row['SP1']),
					'ask_volume': int(row['SV1']) * 100
				})

		# 获取现货数据
		with open('data/ETF/' + self.current_date + '/SH510900_' + self.current_date + '.csv', encoding = 'gb2312') as csvfile:
			reader = csv.DictReader(csvfile)
			for row in reader:
				self.stock_data.append({
					'time': row['时间'].split(' ')[1],
					'price': float(row['最新价']),
					'bid_prices': [float(row['买一价']), float(row['买二价']), float(row['买三价']), float(row['买四价']), float(row['买五价'])],
					'bid_volumes': [int(row['买一量']), int(row['买二量']), int(row['买三量']), int(row['买四量']), int(row['买五量'])],
					'ask_prices': [float(row['卖一价']), float(row['卖二价']), float(row['卖三价']), float(row['卖四价']), float(row['卖五价'])],
					'ask_volumes': [int(row['卖一量']), int(row['卖二量']), int(row['卖三量']), int(row['卖四量']), int(row['卖五量'])]
				})

	def get_current_index_data(self, current_time):
		while self.index_pointer + 1 < len(self.index_data) and self.index_data[self.index_pointer + 1]['time'] <= current_time:
			self.index_pointer += 1

		return self.index_data[self.index_pointer]

	def get_current_stock_data(self, current_time):
		while self.stock_pointer + 1 < len(self.stock_data) and self.stock_data[self.stock_pointer + 1]['time'] <= current_time:
			self.stock_pointer += 1				

		return self.stock_data[self.stock_pointer]
		
	def get_current_future_data(self, current_time):
		while self.future_pointer + 1 < len(self.future_data) and self.future_data[self.future_pointer + 1]['time'] <= current_time:
			self.future_pointer += 1			

		return self.future_data[self.future_pointer]
	
	def time_string(self, hour, minute, second):
		temp_str = ''
		if hour < 10:
			temp_str += '0'
		temp_str += str(hour) + ':'	

		if minute < 10:
			temp_str += '0'
		temp_str += str(minute) + ':'

		if second < 10:
			temp_str += '0'
		temp_str += str(second)
		return temp_str

	def calculate_net_worth(self, current_price):
		stock_rate = (current_price - self.index_closes[self.current_date]) / self.index_closes[self.current_date]
		current_net_worth = self.net_worths[self.current_date] * (1 + stock_rate) # 这里默认持仓为满仓状态

		return current_net_worth

	def calculate_estimate_index(self, volume, index_price, current_net_worth, volumes, prices):
		contract_value = Account.future_contract(index_price) * volume
		estimate_index = 0

		factor = 100 * (1 + Account.CN_COMMISSION_CHARGE_RATE)

		# 跨越现货三挡的交易直接放弃
		if contract_value > (volumes[0] * prices[0] + volumes[1] * prices[1]) * factor:
			estimate_price = -1
			estimate_volume = 0
		elif contract_value < volumes[0] * prices[0] * factor:
			estimate_price = prices[0]
			estimate_volume = Account.stock_volume(contract_value, estimate_price)
			estimate_index = index_price * estimate_price / current_net_worth 
		else:
			volume_2 = (self.index_closes[self.current_date] * Account.FUTURE_FACTOR) * Account.EXCHANGE_RATE * volume / self.net_worths[self.current_date] - volumes[0] * 100
			estimate_volume = round((volumes[0] * 100 + volume_2) / 100) * 100
			estimate_price = (volumes[0] * prices[0] * 100 + volume_2 * prices[1]) * (1 + Account.CN_COMMISSION_CHARGE_RATE) / estimate_volume
			estimate_index = index_price * estimate_price / current_net_worth

		return 	estimate_price, estimate_volume, estimate_index		

if __name__ == '__main__':
	data = Data('20151210')
	data.prepare_data()
	print(data.calculate_estimate_index(1, 9539.46, 0.9634507701835802, [2747, 18973, 4451, 3249, 3710], [0.976, 0.975, 0.974, 0.973, 0.972]))
